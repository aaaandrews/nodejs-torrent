'use strict';
const fs = require('fs');
const bencode = require('bencode');
const tracker = require('./utils/tracker');


const torrent = bencode.decode(fs.readFileSync('./torrents/test.torrent'));
const announceList = torrent['announce-list'];
for (let announces of announceList){
    let announce = announces[0]
    let rawUrl = announce.toString('utf-8');
    // 目前只能处理基于udp的tracker
    if(rawUrl.startsWith('udp')){

        tracker.getPeersList(rawUrl , peers => {
            console.log(peers);
        })

    }
    // udp://tracker.opentrackr.org:1337/announce
    // udp://tracker.torrent.eu.org:451/announce
    // udp://tracker.zerobytes.xyz:1337/announce
}


