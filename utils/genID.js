'use strict';
const crypto = require('crypto');

let id = null;

module.exports.getId = function () {
    // singleton

    if (!id) {
        id = crypto.randomBytes(20);
        Buffer.from('-AT0001-').copy(id , 0);
    }
}
