// tracker.js 需要做的事情
// 它可以发送一个连接请求
// 获取这一请求的响应并提取到连接 id
// 通过该连接 id 发送一个 announce 请求 —— 从而告诉 tracker 我们需要的文件是哪个
// 获取 announce 的响应并从中提取 peers list
"use strict";

// dgram 用于管理udp连接实例
const dgram = require('dgram');
const Buffer = require('buffer').Buffer;
// url Parser 用于解析协议、主机名、端口
const urlParser = require('url').parse;

const crypto = require('crypto');

// 发送udp消息
// socket : udp连接套接字 message : 发送消息 rawUrl : 原始URL
function udpSend(socket, message, rawUrl, cb = () => {}) {
    const url = urlParser(rawUrl);
    // 一次性将全部消息发出
    // socket.send 第二个参数 代表缓冲区的起始值偏移量（相对于0），第三个参数代表发送的长度
    // 0 msg.length 代表发送完整的缓冲区
    socket.send(message, 0, message.length, url.port, url.host, () => {
        console.log('sended~~')
    });


}

// 解析响应类型
function respType(resp) {
  // ...
}

// 一个连接请求信息的格式 基于 BEP 协议
// Offset  Size            Name            Value
// 0       64-bit integer  connection_id   0x41727101980
// 8       32-bit integer  action          0 // connect
// 12      32-bit integer  transaction_id  ? // random
// 16
{/* <Buffer 00 00 04 17 27 10 19 80 00 00 00 00 a6 ec 6b 7d> */}


// 建立连接请求
function buildConnReq() {
  // ...
  const buf = Buffer.alloc(16);

  // 从 起始位置 开始 第一部分 连接ID
  // 该方法以大端格式写入一个无符号的32位整数
  // node.js 不支持精确的64位整数
  buf.writeUint32BE(0x417 , 0);
  buf.writeUint32BE(0x27101980 , 4);

  // 偏移量为8的位置开始  第二部分 action
  buf.writeUInt32BE(0 , 8);

  // 偏移量为12的位置开始  第三部分 trx_Id
  // crypto.randomBytes 可以生成一个随机的4字节缓冲区
  crypto.randomBytes(4).copy(buf , 12);

}

// 解析连接响应
// Offset  Size            Name            Value
// 0       32-bit integer  action          0 // connect
// 4       32-bit integer  transaction_id
// 8       64-bit integer  connection_id
// 16
function parseConnResp(resp) {
  // ...
  return {
    action : resp.readUInt32BE(0) ,
    transactionId : resp.readUInt32BE(4) ,
    // Reads an unsigned, big-endian 32-bit integer from buf at the specifiedoffset.
    // buffer.read 无法读取 64位的大整数
    // connectionId : resp.readUInt32BE(8)
    // 直接对缓冲区做切片
    connectionId : resp.slice(8)

  }

}

// 建立announce请求
function buildAnnounceReq(connId) {
  // ...
}

// 解析announce响应
function parseAnnounceResp(resp) {
  // ...
}

module.exports.getPeersList = function (rawUrl , cb) {
  // ...
  // udp4 代表创建基于 ipv4 地址格式的通信对象
  const socket = dgram.createSocket('udp4');

  // udpSend(socket , buildConnReq() , rawUrl);
  udpSend(socket , Buffer.from('hello' , 'utf8') , rawUrl);

  // socket.on('message' , resp => {
  //   if(respType(resp) === 'connect') {

  //     const connResp = parseConnResp(resp);

  //   } else if (respType(response) === 'announce') {

  //     const announceRsp = parseAnnounceResp(resp);

  //     cb(announceRsp.peers);
  //   }
  // });

}

